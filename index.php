<?php

require('ape.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->get_name() . "<br>";
echo "Legs : " . $sheep->get_legs() . "<br>";
echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br><br>";

$frog = new Frog("buduk");

echo "Name : " . $frog->get_name() . "<br>";
echo "Legs : " . $frog->get_legs() . "<br>";
echo "Cold Blooded : " . $frog->get_cold_blooded() . "<br>";
echo "Jump : " . $frog->jump . "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell . "<br>";
